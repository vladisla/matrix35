#include <iostream>
#include <time.h>
#include "matrix.h"

using namespace std;

int main()
{
    srand(time(NULL));

    int m, n;

    cin >> m >> n;

    int** arr1 = new int* [m];
    int** arr2 = new int* [m];

    for (int i = 0; i < m; i++)
    {
        arr1[i] = new int[n];
        arr2[i] = new int[n];

        for (int j = 0; j < n; j++)
        {
            arr1[i][j] = rand() % 10;
            arr2[i][j] = rand() % 10;
        }
    }

    printArray(arr1, m, n);
    cout << endl;
    printArray(arr2, m, n);
    cout << endl;

    int** arr3 = sum(arr1, arr2, m, n);
    printArray(arr3, m, n);
}