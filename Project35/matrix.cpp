#include <iostream>
using namespace std;
int** sum(int** arr1, int** arr2, int m, int n)
{
    int** res = new int* [m];
    for (int i = 0; i < m; i++)
    {
        res[i] = new int[n];
    }

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            res[i][j] = arr1[i][j] + arr2[i][j];
        }
    }

    return res;
}

float** sum(float** arr1, float** arr2, int m, int n)
{
    float** res = new float* [m];
    for (int i = 0; i < m; i++)
    {
        res[i] = new float[n];
    }

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            res[i][j] = arr1[i][j] + arr2[i][j];
        }
    }

    return res;
}

double** sum(double** arr1, double** arr2, int m, int n) {
    double** res = new double* [m];
    for (int i = 0;i < m;i++) {
        res[i] = new double[n];
    }
    for (int i = 0;i < m;i++) {
        for (int j = 0;j < n;j++) {
            res[i][j] = arr1[i][j] + arr2[i][j];
        }
    }
    return res;
}

long double** sum(long double** arr1, long double** arr2, int m, int n) {
    long double** res = new long double* [m];
    for (int i = 0;i < m;i++) {
        res[i] = new long double[n];
    }
    for (int i = 0;i < m;i++) {
        for (int j = 0;j < n;j++) {
            res[i][j] = arr1[i][j] + arr2[i][j];
        }
    }
    return res;
}

void printArray(int** arr, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}
